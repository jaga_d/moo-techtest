package com.moo.techtest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ResultsPage {

    final static String LOC_RESULTS_TEXT = "div#stats-container div.h3";


    final static String LOC_RESULT_PAGES = "#pagination-top > div";
    final static String LOC_RESULT_HITS_EMPTY = "div.ais-hits.ais-hits__empty";

    final static String LOC_RESULT_HITS_ITEM = "div.ais-hits--item";
    final static By BY_RESULT_HITS_ITEM = By.cssSelector(LOC_RESULT_HITS_ITEM);

    final static String LOC_RESULTS_HITS_ITEM_HEADER = "div.u-marginBottom-xxxxs";
    final static String LOC_RESULTS_HITS_ITEM_BODY = "div.media-flex__body";


    static String getItemText(WebElement el) {
        return (getItemHeader(el) + " " + getItemBody(el)).toLowerCase();
    }
    static String getItemHeader(WebElement el) {
        return el.findElement(By.cssSelector(LOC_RESULTS_HITS_ITEM_HEADER)).getText();
    }
    static String getItemBody(WebElement el) {
        return el.findElement(By.cssSelector(LOC_RESULTS_HITS_ITEM_BODY)).getText();
    }

}
