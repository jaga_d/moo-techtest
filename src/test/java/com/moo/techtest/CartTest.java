package com.moo.techtest;

import io.ddavison.conductor.Browser;
import io.ddavison.conductor.Config;
import io.ddavison.conductor.Locomotive;

import org.junit.Test;

@Config(browser = Browser.CHROME,
        url = HomePage.URL)
public class CartTest extends Locomotive {

    @Test
    public void TestThatProductCanBeAddedToCart() {

        logInfo("When user clicks 'Order a free sample pack'");
        click(HomePage.LOC_LNK_SAMPLEPACK);

        logInfo("And clicks on Add to cart button");
        click(HomePage.LOC_LNK_BTN_ADD_TO_CART);

        logInfo("Then cart page is opened");
        validateUrl("cart");

        logInfo("And there is '1' next to cart icon");
        validateText(HomePage.LOC_RESULT_CART_COUNT,"1");
    }

    @Test
    public void TestThatCartWithProductCanBeOpened() {

        logInfo("When Product is added to cart");
        click(HomePage.LOC_LNK_SAMPLEPACK);
        click(HomePage.LOC_LNK_BTN_ADD_TO_CART);

        logInfo("And user navigates to Home Page");
        navigateTo(HomePage.URL);

        logInfo("And user clicks on cart button");
        click(HomePage.LOC_LNK_BTN_CART);

        logInfo("Then cart page is opened");
        validateUrl("cart");
    }
}