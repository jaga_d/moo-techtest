# Test framework for www.moo.com

### Running tests
Required to run these tests are JDK8+ and maven v3.0+.
To run the tests type in project root the following: `mvn test`

##### Following use cases where tested:
 * Searching for 'business cards' produces valid results
 * Seaching for invalid input won't produce results
 * Adding a product to cart is reflected on the cart icon
 * A non-empty cart can be opened from homepage

##### Assumptions
First use case validates actual data on the website.
Because the search is performed using fuzzy searching or more complex methods,
the test could possibly fail in the future, if the underlying data changes.


##### Possible improvements
I have sometimes observed a popup for newsletter signup appearing during a test.
This could be detected and the 'X' icon clicked to not interfere with the scenario.

###### Answers to the questions are in the file `ANSWERS.md`

