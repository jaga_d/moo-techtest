package com.moo.techtest;

import io.ddavison.conductor.Browser;
import io.ddavison.conductor.Config;
import io.ddavison.conductor.Locomotive;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@Config(browser = Browser.CHROME,
        url = HomePage.URL)
public class SearchTest extends Locomotive {

    @Test
    public void testThatSearchBusinessCardsReturnsResults() {
        String searchQuery = "business cards";
        Set<String> acceptedSearchWordsVariations = new HashSet<>();
        acceptedSearchWordsVariations.add("business");
        acceptedSearchWordsVariations.add("businesses");
        acceptedSearchWordsVariations.add("card");
        acceptedSearchWordsVariations.add("cards");

        logInfo("When user types '" + searchQuery + "' into search field");
        setText(HomePage.LOC_INP_SEARCHFIELD, searchQuery);

        logInfo("And user clicks search button");
        click(HomePage.LOC_LNK_SEARCHBUTTON);

        logInfo("Then results page is opened");
        validateUrl("https\\:\\/\\/www\\.moo\\.com\\/uk\\/search\\.html\\?query\\=business\\+cards");

        logInfo("And there is text 'Results for \"" + searchQuery + "\"'");
        validateText(ResultsPage.LOC_RESULTS_TEXT, "Results for “" + searchQuery + "”");

        logInfo("And pagination is displayed");
        validateAttribute(ResultsPage.LOC_RESULT_PAGES, "style", "");

        logInfo("And sees a view of products matching term '" + searchQuery + "'");
        List<WebElement> hits = driver.findElements(ResultsPage.BY_RESULT_HITS_ITEM);

        for (WebElement el : hits) {
            String textToVerify = ResultsPage.getItemText(el);

            assertThat(acceptedSearchWordsVariations).containsAnyOf(textToVerify.split(" "));
        }

        logInfo("And there is at least 1 such result");
        assertThat(hits.size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    public void testThatInvalidProductSearchDoesntReturnResults() {
        String searchQuery = "sdjfnjsdfj";

        logInfo("When user types '" + searchQuery + "' into search field");
        setText(HomePage.LOC_INP_SEARCHFIELD, searchQuery);

        logInfo("And user clicks search button");
        click(HomePage.LOC_LNK_SEARCHBUTTON);

        logInfo("Then results page is opened");
        validateUrl("https\\:\\/\\/www\\.moo\\.com\\/uk\\/search\\.html\\?query\\=sdjfnjsdfj");

        logInfo("And there is text 'Results for \"" + searchQuery + "\"'");
        validateText(ResultsPage.LOC_RESULTS_TEXT, "Results for “" + searchQuery + "”");

        logInfo("And pagination is not displayed");
        validateAttribute(ResultsPage.LOC_RESULT_PAGES, "style", "display\\: none\\;");

        logInfo("And there is no results displayed");
        validatePresent(ResultsPage.LOC_RESULT_HITS_EMPTY);

        List<WebElement> hits = driver.findElements(ResultsPage.BY_RESULT_HITS_ITEM);
        assertThat(hits).isEmpty();

        logInfo("And there is text 'No pages found'");
        validateText(ResultsPage.LOC_RESULT_HITS_EMPTY, "No pages found");
    }
}
