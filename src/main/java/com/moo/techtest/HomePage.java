package com.moo.techtest;

public class HomePage {

    final static String URL = "https://www.moo.com/uk";

    final static String LOC_INP_SEARCHFIELD = "input#query";
    final static String LOC_LNK_SEARCHBUTTON = "button.search__btn";


    final static String LOC_LNK_SAMPLEPACK = "a[data-ga-event-label=\"/uk/free-sample-business-cards.html || Order a free Sample Pack\"]";
    final static String LOC_LNK_BTN_ADD_TO_CART = "button.js-add-accessory-to-cart-button";

    final static String LOC_LNK_BTN_CART = "svg.nav-main__cart-icon";
    final static String LOC_RESULT_CART_COUNT = ".js-cart-count";


}
