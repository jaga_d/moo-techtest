1. What did you think of the provided acceptance criteria?

Some of the criteria were not very clear to me. I had to make some assumptions like i.e. I wasn't sure how to test fuzzy search. In normal working conditions, I would ask questions to specify the requirements.

2. If this were a real story from a real life Product Owner - what questions would you ask?

I would ask to make below phrases (from scenarios) more precise:
"valid product", "invalid product"
 - what does it mean that product is valid, list of valid products
 - invalid products = unintelligible input?
 - how about invalid products but valid search input like "jobs", "CEO", "community"
"view of products"
- how are the results sorted?
"matching that search term"
- how the fuzzy search works? (i.e. when searching for "sticker" then there are in result "stickers" and "label")
"message telling them"
- what exactly should be returned, are the results for products different to other intelligible results

3. Why did you choose to structure your code in the way you did?
I used Selenium framework with Conductor. I used PageObject design pattern to separate web page selectors from behaviour description in tests.
I described the test scenarios in logs in BDD fashion.

4. If you had more time what improvements would you make to your code?
- I would gladly use BDD cucumber framework to structure my tests and create living documentation
- "business cards" could be used as parameter and then different input could be passed to same functions
- if the framework is supposed to be developed then I would make more reusable methods
- I would try to solve the problem with "newsletter signup" popup

5. What is your usual approach to testing on a project? (Hint: talk about different levels of testing you would do; who would collaborate with whom etc)

Firstly I would establish what functionality I should test i.e. if the tests should be more presentation of results or how the search engine works.
Then I would analise the requirements and think about test cases that would test the funtionality. Think about edge cases etc usually generates lots of questions to product owner/business analist. I'm trying to do this ASAP because usually my questions help developers as well - they consider less common scenarios in earlier phase of their work.
From my experience the decision about how the solutions is delivered is up to developers so then I would collaborate closely with them to understand how it's done.
